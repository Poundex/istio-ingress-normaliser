package net.poundex.k8s.istioingress;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationPropertiesScan
@EnableRetry
public class IstioIngressNormaliserApplication {

	public static void main(String[] args) {
		SpringApplication.run(IstioIngressNormaliserApplication.class, args);
	}

}
