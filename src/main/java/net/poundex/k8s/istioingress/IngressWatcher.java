package net.poundex.k8s.istioingress;

import io.fabric8.kubernetes.api.model.networking.v1.Ingress;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.informers.ResourceEventHandler;
import io.fabric8.kubernetes.client.informers.SharedIndexInformer;
import io.fabric8.kubernetes.client.informers.SharedInformerFactory;
import io.fabric8.kubernetes.client.informers.cache.Cache;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@RequiredArgsConstructor
@Slf4j
@Component
class IngressWatcher implements ResourceEventHandler<Ingress>, Runnable, ApplicationListener<ContextRefreshedEvent> {
	
	private final KubernetesClient kubernetesClient;
	private final IngressService ingressService;
	
	private final BlockingQueue<String> workqueue = new ArrayBlockingQueue<>(1024);
	private final Executor executor = Executors.newSingleThreadExecutor();
	
	private SharedIndexInformer<Ingress> sharedIndexInformer;
	private SharedInformerFactory informerFactory;

	@PostConstruct
	public void init() throws ExecutionException, InterruptedException {
		informerFactory = kubernetesClient.informers();

		sharedIndexInformer = informerFactory.sharedIndexInformerFor(
				Ingress.class, Duration.ofMinutes(10).toMillis());

		informerFactory.addSharedInformerEventListener(exception -> log.error("Exception occurred, but caught", exception));
	}

	@Override
	public void onAdd(Ingress ingress) {
		enqueueIngress(ingress);
	}

	@Override
	public void onUpdate(Ingress old, Ingress ingress) {
		enqueueIngress(ingress);
	}

	@Override
	public void onDelete(Ingress obj, boolean deletedFinalStateUnknown) {
	}
	
	private void enqueueIngress(Ingress ingress) {
		log.info("enqueue {}", ingress.getMetadata().getName());
		String key = Cache.metaNamespaceKeyFunc(ingress);
		log.info("Going to enqueue key {}", key);
		if (key != null && ! key.isEmpty()) {
			log.info("Adding item to workqueue");
			workqueue.add(key);
		}
	}

	@Override
	public void run() {
		log.info("Starting {} controller", Ingress.class.getSimpleName());
		log.info("Waiting for informer caches to sync");
		while ( ! sharedIndexInformer.hasSynced()) {
			// Wait till Informer syncs
		}

		while ( ! Thread.currentThread().isInterrupted()) {
			try {
				doRun();
			} catch (InterruptedException interruptedException) {
				Thread.currentThread().interrupt();
				log.error("controller interrupted..");
			}
		}
		
		System.exit(1);
	}

	private void doRun() throws InterruptedException {
		log.info("trying to fetch item from workqueue...");
		if (workqueue.isEmpty()) 
			log.info("Work Queue is empty");
		
		String key = workqueue.take();
		Objects.requireNonNull(key, "key can't be null");
		
		log.info("Got {}", key);
		if ( ! key.contains("/"))
			log.warn("invalid resource key: {}", key);
		

		String[] bits = key.split("/");
		Ingress ingress = kubernetesClient.network().v1().ingresses()
				.inNamespace(bits[0])
				.withName(bits[1]).get();
		if (ingress == null) 
			log.error("Ingress {}/{} in workqueue no longer exists", bits[0], bits[1]);
		
		ingressService.createForIngress(ingress);
	}

	@SneakyThrows
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		Future<Void> startInformersFuture = informerFactory.startAllRegisteredInformers();
		startInformersFuture.get();

		sharedIndexInformer.addEventHandler(this);
		executor.execute(this);
	}
}
