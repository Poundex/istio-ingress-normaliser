package net.poundex.k8s.istioingress;

import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
@RequiredArgsConstructor
class ContextConfig {
	
	private final ConfigProperties config;
	
	@Bean
	public KubernetesClient kubernetesClient() throws IOException {
		KubernetesClientBuilder builder = new KubernetesClientBuilder();
		
		if(StringUtils.hasText(config.getKubeconfig()))
				builder.withConfig(Config.fromKubeconfig(
						Files.readString(Paths.get(config.getKubeconfig()))));
		
		return builder.build();
	}
}
