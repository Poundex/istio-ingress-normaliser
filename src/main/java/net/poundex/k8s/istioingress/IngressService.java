package net.poundex.k8s.istioingress;

import io.fabric8.kubernetes.api.model.networking.v1.Ingress;
import io.fabric8.kubernetes.api.model.networking.v1.IngressRule;
import io.fabric8.kubernetes.api.model.networking.v1.IngressServiceBackend;
import io.fabric8.kubernetes.client.KubernetesClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
class IngressService {
	
	private final KubernetesClient kubernetesClient;
	private final ConfigProperties configProperties;

	@Retryable(backoff = @Backoff(multiplier = 5.0))
	public void createForIngress(Ingress ingress) {
		createGateway(ingress);
		createVirtualService(ingress);
	}

	private void createGateway(Ingress ingress) {
		if(StringUtils.hasText(configProperties.getSingleGateway()))
			return;
		
		kubernetesClient.resource(String.format("""
								apiVersion: %s
								kind: %s
								metadata:
								  name: %s
								  namespace: %s
								spec:
								  selector:
								    istio: %s
								  servers:
								  - port:
								      number: %s
								      name: %s
								      protocol: %s
								    hosts:
								    - "%s"
								    %s
								""",
						IstioResources.Gateway.getApiVersion(),
						IstioResources.Gateway.getKind(),
						ingress.getMetadata().getName(),
						ingress.getMetadata().getNamespace(),
						configProperties.getSelector(),
						configProperties.getCredential() == null ? "80" : "443",
						configProperties.getCredential() == null ? "http" : "https",
						configProperties.getCredential() == null ? "HTTP" : "HTTPS",
						ingress.getSpec().getRules().get(0).getHost(),
						getCert()))
				.createOrReplace();
	}
	
	private void createVirtualService(Ingress ingress) {
		kubernetesClient.resource(String.format("apiVersion: %s\n" +
								"kind: %s\n" +
								"metadata:\n" +
								"  name: %s\n" +
								"  namespace: %s\n" +
								"spec:\n" +
								"  hosts:\n" +
								"    - \"%s\"\n" +
								"  gateways:\n" +
								"    - %s\n" +
								"  http:\n" +
								"%s\n",
						IstioResources.VirtualService.getApiVersion(),
						IstioResources.VirtualService.getKind(),
						ingress.getMetadata().getName(),
						ingress.getMetadata().getNamespace(),
						ingress.getSpec().getRules().get(0).getHost(),
						StringUtils.hasText(configProperties.getSingleGateway())
								? configProperties.getSingleGateway()
								: ingress.getMetadata().getName(),
						getRules(ingress.getMetadata().getNamespace(), ingress.getSpec().getRules(), ingress.getMetadata().getAnnotations())))
				.createOrReplace();
	}

	private String getPort(String namespace, IngressRule ingressRule) {
		IngressServiceBackend svc = ingressRule.getHttp().getPaths().get(0).getBackend().getService();
		if(svc.getPort().getNumber() != null)
			return svc.getPort().getNumber().toString();

		return kubernetesClient.services().inNamespace(namespace)
				.withName(svc.getName()).get().getSpec().getPorts().stream()
				.filter(p -> p.getName().equals(svc.getPort().getName()))
				.findFirst()
				.get().getPort().toString();
	}

	private String getRules(String namespace, List<IngressRule> rules, Map<String, String> annotations) {
		StringBuilder sb = new StringBuilder();
		rules.forEach(ingressRule -> {
			ingressRule.getHttp().getPaths().get(0).getBackend().getService().getName();
			sb.append(String.format("    - match:\n" +
			                        "      - uri:\n" +
			                        "          prefix: %s\n" +
									"%s\n" +
			                        "      route:\n" +
			                        "        - destination:\n" +
			                        "            port:\n" +
			                        "              number: %s\n" +
			                        "            host: %s\n", 
					ingressRule.getHttp().getPaths().get(0).getPath(),
					getRewrite(annotations),
					getPort(namespace, ingressRule),
					ingressRule.getHttp().getPaths().get(0).getBackend().getService().getName()));
		});
		return sb.toString();
	}

	private String getRewrite(Map<String, String> annotations) {
		if (! annotations.containsKey("ingress-normaliser/rewrite"))
			return "";

		return String.format(
				"      rewrite:\n" + 
				"        uri: %s\n",
				annotations.get("ingress-normaliser/rewrite"));
	}

	private String getCert() {
		if( ! StringUtils.hasText(configProperties.getCredential()))
			return "";
		
		return "tls:\n" +
				"      mode: SIMPLE\n" +
				"      credentialName: " + configProperties.getCredential() + "\n";
	}
}
