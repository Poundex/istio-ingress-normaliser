package net.poundex.k8s.istioingress;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
enum IstioResources {

	Gateway("networking.istio.io/v1alpha3", "Gateway"),
	VirtualService("networking.istio.io/v1alpha3", "VirtualService");
	
	private final String apiVersion;
	private final String kind;
	
}
