package net.poundex.k8s.istioingress;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("ingress")
@Data
class ConfigProperties {
	private String kubeconfig;
	private String selector;
	private String credential;
	private String singleGateway;
}
